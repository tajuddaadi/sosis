import React from 'react';
import { StyleSheet, View, Button, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import Screen1 from './screens/Screen1';
import Screen2 from './screens/Screen2';
import Screen3 from './screens/Screen3';
import Screen4 from './screens/Screen4';
import Screen5 from './screens/Screen5';
import RumSakit from './screens/detailScreen1/RumSakit';
import Pusk from './screens/detailScreen1/Pusk';
import Derek from './screens/detailScreen5/Derek';
import Tambal from './screens/detailScreen5/Tambal';

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator({
  HomeScreen: { screen: HomeScreen },
  Screen1: { screen: Screen1 },
  Screen2: { screen: Screen2 },
  Screen3: { screen: Screen3 },
  Screen4: { screen: Screen4 },
  Screen5: { screen: Screen5 },
  RumSakit: { screen: RumSakit },
  Pusk: { screen: Pusk },
  Derek: { screen: Derek },
  Tambal: { screen: Tambal },
  First: {
      screen: HomeScreen,
    },
  },{
    defaultNavigationOptions: {
      header: null
    },
});

const AppContainer = createAppContainer(AppNavigator);