import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, Alert,TouchableHighlight,
  ImageBackground,
  ScrollView,
} from 'react-native';
import styles from '../style';
import call from 'react-native-phone-call';

export default class RumSakit extends Component {
  button1 = () => {
    const args = {
      number: '634634',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Rumah Sakit Moewardi",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button2 = () => {
    const args = {
      number: '643013',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Rumah Sakit Kustati",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button3 = () => {
    const args = {
      number: '656903',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Rumah Sakit Triharsi",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button4 = () => {
    const args = {
      number: '714422',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Rumah Sakit Kasih Ibu",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button5 = () => {
    const args = {
      number: '714578',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S. PKU Muhammadiyah ",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button6 = () => {
    const args = {
      number: '710710',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S. Brayat Minulya",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button7 = () => {
    const args = {
      number: '620620',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S. Dr Oen Solo Baru",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button8 = () => {
    const args = {
      number: '643139',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S. Dr Oen Kandang Sapi",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button9 = () => {
    const args = {
      number: '712077',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S. Panti Waluyo",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button10 = () => {
    const args = {
      number: '715500',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S.U Daerah Surakarta",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button11 = () => {
    const args = {
      number: '646505',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Palang Merah Surakarta",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button12 = () => {
    const args = {
      number: '641422',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon R.S. Jiwa Daerah Surakarta",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight
            onPress={this.button1}>
            <ImageBackground
              source={require('../../assets/download.jpg')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Moewardi</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button2}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Kustati</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button3}>
            <ImageBackground
              source={require('../../assets/rs.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Triharsi</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button4}>
            <ImageBackground
              source={require('../../assets/download.jpg')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Kasih Ibu</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button5}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>R.S. PKU Muhammadiyah</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button6}>
            <ImageBackground
              source={require('../../assets/rs.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Brayat Minulya</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button7}>
            <ImageBackground
              source={require('../../assets/download.jpg')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Dr Oen Solo Baru</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button8}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>R.S. Dr Oen Kandang Sapi</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button9}>
            <ImageBackground
              source={require('../../assets/rs.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Panti Waluyo</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button10}>
            <ImageBackground
              source={require('../../assets/download.jpg')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Umum Surakarta</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button11}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Palang Merah Surakarta</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button12}>
            <ImageBackground
              source={require('../../assets/rs.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit Jiwa Surakarta</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}