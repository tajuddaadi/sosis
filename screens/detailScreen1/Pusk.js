import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, Alert,TouchableHighlight,
  ImageBackground,
  ScrollView,
} from 'react-native';
import styles from '../style';
import call from 'react-native-phone-call';

export default class Pusk extends Component {
  button1 = () => {
    const args = {
      number: '641257',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Jayengan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button2 = () => {
    const args = {
      number: '655539',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Kratonan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button3 = () => {
    const args = {
      number: '654077',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Gajahan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button4 = () => {
    const args = {
      number: '655061',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Sangkrah",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button5 = () => {
    const args = {
      number: '714832',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Penumping",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button6 = () => {
    const args = {
      number: '716333',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Purwosari",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button7 = () => {
    const args = {
      number: '714594',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Pajang",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button8 = () => {
    const args = {
      number: '647545',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Purwodiningratan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button9 = () => {
    const args = {
      number: '641033',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Setabelan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button10 = () => {
    const args = {
      number: '854252',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Sibela",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button11 = () => {
    const args = {
      number: '646919',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Ngoresan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button12 = () => {
    const args = {
      number: '719313',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Manahan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button13 = () => {
    const args = {
      number: '637025',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Gilingan",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button14 = () => {
    const args = {
      number: '711244',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Puskesmas Banyuanyar",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight
            onPress={this.button1}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Jayengan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button2}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Kratonan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button3}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Gajahan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button4}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Sangkrah</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button5}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Penumping</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button6}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Purwosari</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button7}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Pajang</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button8}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Purwodiningratan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button9}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Setabelan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button10}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Sibela</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button11}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Ngoresan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button12}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Manahan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button13}>
            <ImageBackground
              source={require('../../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Gilingan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button14}>
            <ImageBackground
              source={require('../../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas Banyuanyar</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}