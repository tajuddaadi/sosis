import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ImageBackground,
  View,
  Button,
  Alert,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
import styles from './style1';

export default class Screen1 extends Component {
  _onPressButton() {
    Alert.alert('You tapped the button!');
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('RumSakit')}>
            <ImageBackground
              source={require('../assets/rs.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Rumah Sakit</Text>
                <Text style={styles.subtitle}>Ambulance Rumah Sakit</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Pusk')}>
            <ImageBackground
              source={require('../assets/puskesmas.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Puskesmas</Text>
                <Text style={styles.subtitle}>Ambulance Puskesmas</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}
