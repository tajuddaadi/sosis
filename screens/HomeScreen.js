import React from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  ImageBackground,
  ScrollView,
} from 'react-native';
import styles from './style';
export default class HomeScreen extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Screen1')}>
            <ImageBackground
              source={require('../assets/ambulance.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Darurat Medis</Text>
                <Text style={styles.subtitle}>Hubungi Ambluance untuk Keadaan Darurat</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Screen2')}>
            <ImageBackground
              source={require('../assets/damkar.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Darurat Kebakaran</Text>
                <Text style={styles.subtitle}>Hubungi Damkar untuk Keadaan Darurat</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Screen3')}>
            <ImageBackground
              source={require('../assets/polisi.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Ketertiban dan Keamanan</Text>
                <Text style={styles.subtitle}>Untuk Keadaan Keamanan Darurat</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Screen4')}>
            <ImageBackground
              source={require('../assets/pln.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Darurat Listrik</Text>
                <Text style={styles.subtitle}>Hubungi PLN</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Screen5')}>
            <ImageBackground
              source={require('../assets/derek.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Darurat Kendaraan</Text>
                <Text style={styles.subtitle}>Untuk Keadaan Darurat Kendaraan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}
