import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  Alert,
  TouchableHighlight,
  ImageBackground,
  ScrollView,
} from 'react-native';
import styles from './style';
import call from 'react-native-phone-call';

export default class Screen3 extends Component {
  button1 = () => {
    const args = {
      number: '712600',
      prompt: false,
    };
    Alert.alert(
      'Telpon Sentral Pelayanan Kepolisian Surakarta',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  button2 = () => {
    const args = {
      number: '644400',
      prompt: false,
    };
    Alert.alert(
      'Telpon Polsekta Banjarsari',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  button3 = () => {
    const args = {
      number: '644506',
      prompt: false,
    };
    Alert.alert(
      'Telpon Polsekta Jebres',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  button4 = () => {
    const args = {
      number: '713500',
      prompt: false,
    };
    Alert.alert(
      'Telpon Polsekta Laweyan',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  button5 = () => {
    const args = {
      number: '633506',
      prompt: false,
    };
    Alert.alert(
      'Telpon Polsekta Pasar Kliwon',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  button6 = () => {
    const args = {
      number: '646506',
      prompt: false,
    };
    Alert.alert(
      'Telpon Polsekta Serengan',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  button7 = () => {
    const args = {
      number: '656969',
      prompt: false,
    };
    Alert.alert(
      'Telpon Satlantas Polresta Solo',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight onPress={this.button1}>
            <ImageBackground
              source={require('../assets/police.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Sentral Pelayanan </Text>
                <Text style={styles.title}>Kepolisian Surakarta</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.button2}>
            <ImageBackground
              source={require('../assets/line.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Polsekta Banjarsari</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.button3}>
            <ImageBackground
              source={require('../assets/police.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Polsekta Jebres</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.button4}>
            <ImageBackground
              source={require('../assets/line.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Polsekta Laweyan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.button5}>
            <ImageBackground
              source={require('../assets/police.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Polsekta Pasar Kliwon</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.button6}>
            <ImageBackground
              source={require('../assets/line.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Polsekta Serengan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.button7}>
            <ImageBackground
              source={require('../assets/police.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Satlantas Polresta Solo</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}
