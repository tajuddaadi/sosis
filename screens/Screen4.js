import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  Alert,
  ScrollView,
  StatusBar,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import call from 'react-native-phone-call';
const { width } = Dimensions.get('window');

export default class Screen4 extends Component {
  button1 = () => {
    const args = {
      number: '722091',
      prompt: false,
    };
    Alert.alert(
      'Telpon PLN Surakarta',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };

  button2 = () => {
    const args = {
      number: '123',
      prompt: false,
    };
    Alert.alert(
      'Telpon PLN Surakarta',
      'Anda yakin ingin menelpon?',
      [
        { text: 'Later', onPress: () => console.log('later pressed') },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log(call(args).catch(console.error)),
        },
      ],
      { cancelable: false }
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={require('../assets/listrik.png')} />
        </View>
        <Image style={styles.avatar} source={require('../assets/call.png')} />
        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text style={styles.name}>PLN Surakarta</Text>

            <Text style={styles.description}>
              Masyarakat dapat menhgubungi layanan PLN ini apabila terjadi
              peristiwa seperti pemadaman listrik karena kerusakan jaringan,
              adanya pohon tumbang yang memutus kabel listrik, dan lain
              sebagainya.
            </Text>
            <Text style={styles.info}>Hubungi Sekarang</Text>
            <TouchableOpacity
              style={styles.buttonContainer}
              onPress={this.button1}>
              <Text>Nomor 1</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.button2}
              style={styles.buttonContainer}>
              <Text>Nomor 2</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#F22630',
    height: 200,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: 'white',
    marginBottom: 10,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: 130,
  },
  name: {
    fontSize: 28,
    color: '#00BFFF',
    fontWeight: '600',
  },
  body: {
    marginTop: 40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding: 30,
  },
  info: {
    fontSize: 16,
    color: '#F22630',
    marginTop: 10,
  },
  description: {
    fontSize: 16,
    color: '#696969',
    marginTop: 10,
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: '#F22630',
  },
});
