import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, Alert,TouchableHighlight,
  ImageBackground,
  ScrollView,
} from 'react-native';
import styles from '../style';
import call from 'react-native-phone-call';

export default class Tambal extends Component {
  button1 = () => {
    const args = {
      number: '085725458598',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Tambal Ban ini",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button2 = () => {
    const args = {
      number: '085642413418',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Tambal Ban ini",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button3 = () => {
    const args = {
      number: '087835461571',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Tambal Ban ini",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button4 = () => {
    const args = {
      number: '087812712128',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Tambal Ban ini",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button5 = () => {
    const args = {
      number: '085326602312',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Tambal Ban ini",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  button6 = () => {
    const args = {
      number: '085867711759',
      prompt: false,
    };
    Alert.alert(
      
      "Telpon Tambal Ban ini",
      "Anda yakin ingin menelpon?",
      [
        { text: "Later", onPress: () => console.log("later pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log(call(args).catch(console.error)) }
      ],
      { cancelable: false }
    );
  };
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight
            onPress={this.button1}>
            <ImageBackground
              source={require('../../assets/tire2.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Tri Cahyono</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button2}>
            <ImageBackground
              source={require('../../assets/tire1.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Bejo</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button3}>
            <ImageBackground
              source={require('../../assets/download.jpg')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Sahono</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button4}>
            <ImageBackground
              source={require('../../assets/tire2.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Murwanto</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button5}>
            <ImageBackground
              source={require('../../assets/download.jpg')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Agung</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={this.button6}>
            <ImageBackground
              source={require('../../assets/tire1.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Japi</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}