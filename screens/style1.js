import { StyleSheet, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    flex : 1
  },
  listitem: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    height: 350,
    width,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    paddingLeft: 20,
    paddingBottom: 20,
    paddingRight: 20
  },
  title: {
    fontSize: 30,
    marginBottom: 0,
    textAlign: 'left',
    color: '#fff',
    fontWeight: '800',
    fontFamily: 'Avenir'
  },
  subtitle: {
    marginBottom: 4,
    color: '#cbced3'
  },
  backgroundImage: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 350,
  },
  price: {
    position:'relative',
    top: 0,
    alignSelf: 'flex-start',
    color: '#00ace6',
    fontWeight: '800',
    fontFamily: 'Avenir'
  }
});