import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ImageBackground,
  View,
  Button,
  Alert,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
import styles from './style1';

export default class Screen5 extends Component {
  _onPressButton() {
    Alert.alert('You tapped the button!');
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Derek')}>
            <ImageBackground
              source={require('../assets/derek.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Derek</Text>
                <Text style={styles.subtitle}>Darurat Derek Kendaraan</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Tambal')}>
            <ImageBackground
              source={require('../assets/tire1.png')}
              style={styles.backgroundImage}>
              <View style={styles.listitem}>
                <Text style={styles.title}>Tambal Ban</Text>
                <Text style={styles.subtitle}>Darurat Ban Bocor</Text>
              </View>
            </ImageBackground>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}
